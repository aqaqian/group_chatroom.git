package com.xiaoxiaozhang.chatroom.service;

import com.xiaoxiaozhang.chatroom.center.UserCenter;
import com.xiaoxiaozhang.chatroom.core.IMessagePublisher;
import com.xiaoxiaozhang.chatroom.dao.MessageDao;
import com.xiaoxiaozhang.chatroom.encoder.MessageEncoder;
import com.xiaoxiaozhang.chatroom.endpoint.MessageEndpoint;
import com.xiaoxiaozhang.chatroom.model.Message;
import com.xiaoxiaozhang.chatroom.model.OnlineUser;
import com.xiaoxiaozhang.chatroom.model.User;

import javax.swing.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 14:21
 */
public class MessageService implements IMessagePublisher {
    private final MessageDao messageDao = MessageDao.getInstance();
    private final UserCenter userCenter = UserCenter.getInstance();
    private final MessageEncoder messageEncoder = MessageEncoder.getInstance();

    private MessageService() {
        System.out.println("MessageService()");
    }
    private static final MessageService instance = new MessageService();
    public static MessageService getInstance() {
        return instance;
    }

    @Override
    public void publish(User user, String content) throws IOException {
        //1,保存消息到数据库中，同时得到生成的消息mid
        Message message = new Message();
        message.uid = user.uid;
        message.nickname = user.nickname;
        message.content = content;
        message.publishedAt = LocalDateTime.now();

        messageDao.insert(message);

        //2,从用户中心中获取当前在线用户
        String messageText = messageEncoder.encoder(message);
        List<OnlineUser> onlineUserList = userCenter.getOnlineUserList();
        for (OnlineUser onlineUser : onlineUserList) {
            onlineUser.send(messageText);
        }
    }
}
