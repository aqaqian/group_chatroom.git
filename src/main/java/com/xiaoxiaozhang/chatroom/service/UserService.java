package com.xiaoxiaozhang.chatroom.service;

import com.xiaoxiaozhang.chatroom.center.UserCenter;
import com.xiaoxiaozhang.chatroom.core.IOnlineUserRegistry;
import com.xiaoxiaozhang.chatroom.dao.MessageDao;
import com.xiaoxiaozhang.chatroom.dao.UserDao;
import com.xiaoxiaozhang.chatroom.encoder.MessageEncoder;
import com.xiaoxiaozhang.chatroom.model.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 10:33
 */
public class UserService implements IOnlineUserRegistry {
    //处理单例
    private final UserCenter userCenter = UserCenter.getInstance();
    private final UserDao userDao = UserDao.getInstance();
    private final MessageDao messageDao = MessageDao.getInstance();
    private final MessageEncoder messageEncoder = MessageEncoder.getInstance();

    private static final UserService instance = new UserService();
    public static UserService getInstance() {
        return instance;
    }
    private UserService() {
        System.out.println("UserService()");
    }

    @Override
    public void online(OnlineUser user) throws IOException {
        //1，查询历史消息并发送
        if (!user.isNewUser()) {
            // 如果不是新用户，得到用户信息
            User userDo = user.getDo();
            List<Message> messageList = messageDao.selectListAfter(userDo.logoutAt);
            //把得到的每条消息都依次发送给刚登陆的用户
            for (Message message : messageList) {
                String messageText = messageEncoder.encoder(message);
                user.send(messageText);
            }
        }
        //2，是新用户，登记用户上线
        userCenter.online(user);
    }

    @Override
    public void offline(OnlineUser user) {
        //1,更新当前用户的登出时间------从OnlineUser这个业务对象中获取到数据库对应的对象（DO)
        User userDo = user.getDo();
        userDo.logoutAt = LocalDateTime.now();
        userDao.updateLogoutAt(userDo);

        //2,登记用户下线
        userCenter.offline(user);
    }


    //注册方法（不考录密码加密）
    public User register(String username, String nickname, String password) {
        User user = new User();
        user.username = username;
        user.nickname = nickname;
        user.password = password;
        user.logoutAt = null;

        // //将新注册的用户信息插入到userDao中进行数据管理
        userDao.insert(user);
        return user;
    }

    //登录方法
    public User login(String username, String password) {
        User user = userDao.selectOneByUsername(username);//从userDao里根据用户名查找用户信息
        if (user == null) {
            return null;
        }
        if (user.password.equals(password)) {
            return user; //只有用户密码正确的情况下才返回该用户信息
        }
        return null;
    }

    public UserListResult getUserList() {
        UserListResult result = new UserListResult();
        //从数据库中读取所有用户   and   从UserCenter中读取在线用户
        List<User> allUserList = userDao.selectAllList();
        List<OnlineUser> onlineUserList = userCenter.getOnlineUserList();

        Set<User> onlineUserSet = new HashSet<>();
        for (OnlineUser onlineUser : onlineUserList) {
            onlineUserSet.add(onlineUser.getDo());
        }

        result.onlineCount = onlineUserSet.size();
        result.totalCount = allUserList.size();
        for (User user : allUserList) {
            UserListUser ulu = new UserListUser();
            ulu.uid = user.uid;
            ulu.nickname = user.nickname;
            if (onlineUserSet.contains(user)) {
                ulu.online = true;
            } else {
                ulu.online = false;
            }
            result.userList.add(ulu);
        }

        //排序
        Collections.sort(result.userList);
        return result;

    }
}
