package com.xiaoxiaozhang.chatroom.model;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-02
 * Time: 0:19
 */
public class UserListUser implements Comparable<UserListUser> {
    public Integer uid;
    public String nickname;
    public boolean online;

    @Override
    public int compareTo(UserListUser o) {
        // 比较this和o的大小
        if (this.online != o.online) {
            if (this.online) {
                return -1; //谁在线谁小
            } else {
                return 1;
            }
        }
        //在线状态相等，以uid进行比较
        return this.uid - o.uid;
    }
}
