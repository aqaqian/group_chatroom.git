package com.xiaoxiaozhang.chatroom.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 11:07
 */
public class Message {
    public Integer mid;
    public Integer uid;
    public String nickname;
    public String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime publishedAt;
}
