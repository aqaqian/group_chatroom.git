package com.xiaoxiaozhang.chatroom.center;

import com.xiaoxiaozhang.chatroom.core.IOnlineUserRegistry;
import com.xiaoxiaozhang.chatroom.model.OnlineUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 10:27
 */
public class UserCenter implements IOnlineUserRegistry {
    private UserCenter() {
        System.out.println("UserCenter()");
    }
    //保证UserCenter的单例，用private封装，不允许其他代码调用
    //用static修饰保证全局唯一，无论谁使用UserCenter对象，只能通过UserCenter.getInst
    private static final UserCenter instance = new UserCenter();
    public static UserCenter getInstance() {
        return instance;
    }

    //创建一个在线用户列表
    private final List<OnlineUser> onlineUserList = new ArrayList<>();
    @Override
    public void online(OnlineUser user) throws IOException {
        if (onlineUserList.contains(user)) {
            //如果该用户已经在线了，不允许多次登录，所以把上次登录的用户踢下线
            int i = onlineUserList.indexOf(user);
            OnlineUser prevUser = onlineUserList.get(i);
            prevUser.kick();
            return;
        }
        //此前处于不在线状态，将该用户添加到在线列表
        onlineUserList.add(user);
        System.out.println("在线用户: " + onlineUserList);
    }

    @Override
    public void offline(OnlineUser user) {
        onlineUserList.remove(user);
        System.out.println("在线用户: " + onlineUserList);

    }

    public List<OnlineUser> getOnlineUserList() {
        return new ArrayList<>(onlineUserList);
    }
}
