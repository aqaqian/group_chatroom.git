package com.xiaoxiaozhang.chatroom.encoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.xiaoxiaozhang.chatroom.model.Message;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 11:21
 */
public class MessageEncoder {
    private static final MessageEncoder instance = new MessageEncoder();
    public static MessageEncoder getInstance() {
        return instance;
    }

    private final ObjectMapper om = new ObjectMapper();

    private MessageEncoder() {
        System.out.println("MessageEncoder()");
        om.registerModule(new JavaTimeModule());
    }

    public String encoder(Message message) throws JsonProcessingException {
        return om.writeValueAsString(message);
    }
}
