package com.xiaoxiaozhang.chatroom.endpoint;

import com.xiaoxiaozhang.chatroom.model.OnlineUser;
import com.xiaoxiaozhang.chatroom.model.User;
import com.xiaoxiaozhang.chatroom.service.MessageService;
import com.xiaoxiaozhang.chatroom.service.UserService;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;

import static javax.websocket.CloseReason.CloseCodes.NORMAL_CLOSURE;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 11:26
 */
//websocket在建立连接时，会构造HttpSessionConfigurator对象，并且调用其modifyHandshake方法
//先执行modifyHandshake，后执行onOpen
//这里是固定用法
@ServerEndpoint(value = "/message", configurator = HttpSessionConfigurator.class)
public class MessageEndpoint {
    private final UserService userService = UserService.getInstance();
    private final MessageService messageService = MessageService.getInstance();
    private OnlineUser currentUser;

    public MessageEndpoint() {
        System.out.println("MessageEndpoint()");
    }

    //由于onClose的调用肯定是在onOpen之后的，所以可以在onOpen里保存当前用户
    @OnOpen
    public void onOpen(Session session) throws IOException {
        //处理用户上线逻辑
        //1，判断当前上线的是哪个用户————从HttpSession中获取当前登录用户信息
        User user = getCurrentUser(session);
        if (user == null) {
            CloseReason reason = new CloseReason(NORMAL_CLOSURE, "必须先登录");
            session.close(reason);
            return;
        }
        //2，记录该用户到在线用户列表中（如果重复上线，把之前的踢下线）
        currentUser = new OnlineUser(user, session);
        userService.online(currentUser);
    }

    @OnClose
    public void onClose() {
        userService.offline(currentUser);
    }

    @OnMessage
    public void onMessage(String content) throws IOException {
        messageService.publish(currentUser.getDo(), content);
    }

    @OnError
    public void onError(Throwable throwable) {
        throwable.printStackTrace(System.out);
    }

    private User getCurrentUser(Session session) {
        Map<String, Object> userProperties = session.getUserProperties();
        HttpSession httpSession = (HttpSession) userProperties.get("httpSession");
        if (httpSession == null) {
            return null;
        }

        return (User) httpSession.getAttribute("currentUser");
    }
}
